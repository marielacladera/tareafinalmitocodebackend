package com.mitocode.controller;

import com.mitocode.dto.SignosDTO;
import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Signos;
import com.mitocode.service.ISignosService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/signos")
public class SignosController {
    @Autowired
    private ISignosService service;

    @Autowired
    private ModelMapper mapper;

    @GetMapping
    public ResponseEntity<List<SignosDTO>> listar() throws Exception{
        List<SignosDTO> lista = service.listar().stream().map(p -> mapper.map(p, SignosDTO.class)).collect(Collectors.toList());
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    //@RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<SignosDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
        SignosDTO dtoResponse;
        Signos obj = service.listarPorId(id);
        if(obj == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
        }else {
            dtoResponse = mapper.map(obj, SignosDTO.class);
        }
        return new ResponseEntity<>(dtoResponse, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Void> registrar(@Valid @RequestBody SignosDTO dtoRequest) throws Exception {
        Signos s = mapper.map(dtoRequest, Signos.class);
        Signos obj = service.registrar(s);
        SignosDTO dtoResponse = mapper.map(obj, SignosDTO.class);
        URI location =  ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(dtoResponse.getIdSignoVital()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping
    public ResponseEntity<SignosDTO> modificar(@Valid @RequestBody SignosDTO dtoRequest) throws Exception {
        Signos sig = service.listarPorId(dtoRequest.getIdSignoVital());

        if(sig == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdSignoVital());
        }

        Signos s = mapper.map(dtoRequest, Signos.class);
        Signos obj = service.modificar(s);
        SignosDTO dtoResponse = mapper.map(obj, SignosDTO.class);

        return new ResponseEntity<>(dtoResponse, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
        Signos s = service.listarPorId(id);

        if(s == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
        }

        service.eliminar(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/pageable")
    public ResponseEntity<Page<SignosDTO>> listarPageable(Pageable page) throws Exception{
        Page<SignosDTO> signos =service.listarPageable(page).map(s -> mapper.map(s, SignosDTO.class));
        return new ResponseEntity<>(signos, HttpStatus.OK);
    }

}
