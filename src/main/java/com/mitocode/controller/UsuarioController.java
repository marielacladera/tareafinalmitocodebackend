package com.mitocode.controller;

import com.mitocode.dto.UsuarioDTO;
import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;


@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private IUsuarioService service;

    @GetMapping
    public ResponseEntity<List<UsuarioDTO>> listar() throws Exception{
        List<Usuario> usuarios =  service.listar();
        List<UsuarioDTO> responseDTO = modelMapper.map(usuarios, new TypeToken<List<UsuarioDTO>>() {}.getType());
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UsuarioDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
        UsuarioDTO dtoResponse;
        Usuario obj = service.listarPorId(id);
        if(obj == null){
            throw new ModeloNotFoundException("ID DE USUARIO NO ENCONTRADO" + id);
        }else{
            dtoResponse = modelMapper.map(obj, UsuarioDTO.class);
        }
        return new ResponseEntity<>(dtoResponse, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UsuarioDTO> registrar(@Valid @RequestBody UsuarioDTO dtoRequest) throws  Exception{
        Usuario obj = modelMapper.map(dtoRequest, Usuario.class);
        Usuario aux = service.registrar(obj);
        UsuarioDTO dtoResponse = modelMapper.map(aux, UsuarioDTO.class);
        URI location =  ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(dtoResponse.getIdUsuario()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping
    public ResponseEntity<UsuarioDTO>modificar(@Valid @RequestBody UsuarioDTO dtoRequest)throws Exception{
        Usuario obj = service.listarPorId(dtoRequest.getIdUsuario());
        if(obj == null){
            throw new ModeloNotFoundException("ID DE USUARIO NO ENCONTRADO" + dtoRequest.getIdUsuario());
        }
        Usuario m = modelMapper.map(dtoRequest, Usuario.class);
        obj = service.modificar(m);
        UsuarioDTO dtoResponse = modelMapper.map(obj, UsuarioDTO.class);
        return new ResponseEntity<>(dtoResponse, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> eliminarPorId(@PathVariable("id") Integer id) throws Exception{
        Usuario obj = service.listarPorId(id);
        if( obj == null){
            throw new ModeloNotFoundException("ID DE USUARIO NO ENCONTRADO" + id);
        }
        service.eliminar(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}