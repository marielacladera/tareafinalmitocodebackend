package com.mitocode.controller;

import com.mitocode.dto.MenuDTO;
import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Menu;
import com.mitocode.service.IMenuService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/menus")
public class MenuController {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private IMenuService service;

    @GetMapping
    public ResponseEntity<List<MenuDTO>> listar() throws Exception{
        List<Menu> menus = new ArrayList<>();
        menus = service.listar();
        List<MenuDTO> menusDTO = modelMapper.map(menus, new TypeToken<List<MenuDTO>>() {}.getType());
        return new ResponseEntity<>(menusDTO, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MenuDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
        MenuDTO dtoResponse;
        Menu obj = service.listarPorId(id);
        if(obj == null){
            throw new ModeloNotFoundException("ID DE USUARIO NO ENCONTRADO" + id);
        }else{
            dtoResponse = modelMapper.map(obj, MenuDTO.class);
        }
        return new ResponseEntity<>(dtoResponse, HttpStatus.OK);
    }

    @PostMapping("/usuario")
    public ResponseEntity<List<MenuDTO>> listar(@RequestBody String nombre) throws Exception{
        List<Menu> menus = new ArrayList<>();
        //Authentication usuarioLogueado = SecurityContextHolder.getContext().getAuthentication();
        //menus = service.listarMenuPorUsuario(usuarioLogueado.getName());
        menus = service.listarMenuPorUsuario(nombre);
        List<MenuDTO> menusDTO = modelMapper.map(menus, new TypeToken<List<MenuDTO>>() {}.getType());
        return new ResponseEntity<>(menusDTO, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<MenuDTO> registrar(@Valid @RequestBody MenuDTO dtoRequest) throws Exception{
        Menu mAux = modelMapper.map(dtoRequest, Menu.class);
        Menu obj = service.registrar(mAux);
        MenuDTO dtoResponse = modelMapper.map(obj, MenuDTO.class);
        return new ResponseEntity<>(dtoResponse, HttpStatus.OK);
    }

    @PutMapping
    public  ResponseEntity<MenuDTO> modificar(@Valid @RequestBody MenuDTO dtoRequest) throws Exception{
        Menu menuAux = service.listarPorId(dtoRequest.getIdMenu());
        if(menuAux == null){
            throw new ModeloNotFoundException("ID NO ENCONTRADO: " + dtoRequest.getIdMenu());
        }
        Menu menu = modelMapper.map(dtoRequest, Menu.class);
        Menu obj = service.modificar(menu);
        MenuDTO dtoResponse = modelMapper.map(obj, MenuDTO.class);
        return new ResponseEntity<>(dtoResponse, HttpStatus.OK);
    }

    @GetMapping("/pageable")
    public ResponseEntity<Page<MenuDTO>> listarPageable(Pageable page) throws Exception{
        Page<MenuDTO> menus = service.listarPageable(page).map( m -> modelMapper.map(m, MenuDTO.class));
        return new ResponseEntity<>(menus, HttpStatus.OK);
    }
}
