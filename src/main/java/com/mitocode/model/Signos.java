package com.mitocode.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="signos")
public class Signos {
	 @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Integer idSignoVital;

	    @ManyToOne
	    @JoinColumn(name = "id_paciente", nullable = false, foreignKey = @ForeignKey(name = "FK_signos_paciente"))
	    private Paciente paciente;

	    @Column(name = "fecha", nullable = false)
	    private LocalDateTime fecha;

	    @Column(name = "temperatura", nullable = false, length = 20)
	    private String temperatura;

	    @Column(name = "pulso", nullable = false, length = 20)
	    private String pulso;

	    @Column(name = "ritmoCardiaco", nullable = false, length = 20)
	    private String ritmoCardiaco;

	    public Integer getIdSignoVital() {
	        return idSignoVital;
	    }

	    public void setIdSignoVital(Integer idSignoVital) {
	        this.idSignoVital = idSignoVital;
	    }

	    public Paciente getPaciente() {
	        return paciente;
	    }

	    public void setPaciente(Paciente paciente) {
	        this.paciente = paciente;
	    }

	    public LocalDateTime getFecha() {
	        return fecha;
	    }

	    public void setFecha(LocalDateTime fecha) {
	        this.fecha = fecha;
	    }

	    public String getTemperatura() {
	        return temperatura;
	    }

	    public void setTemperatura(String temperatura) {
	        this.temperatura = temperatura;
	    }

	    public String getPulso() {
	        return pulso;
	    }

	    public void setPulso(String pulso) {
	        this.pulso = pulso;
	    }

	    public String getRitmoCardiaco() {
	        return ritmoCardiaco;
	    }

	    public void setRitmoCardiaco(String ritmoCardiaco) {
	        this.ritmoCardiaco = ritmoCardiaco;
	    }
}
