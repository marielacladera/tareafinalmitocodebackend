package com.mitocode.controller;

import com.mitocode.dto.RolDTO;
import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Rol;
import com.mitocode.service.IRolService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/roles")
public class RolController {
    @Autowired
    private IRolService service;

    @Autowired
    private ModelMapper mapper;

    @GetMapping
    public ResponseEntity<List<RolDTO>> listar() throws Exception{
       List<RolDTO> lista = service.listar().stream().map(r -> mapper.map(r, RolDTO.class)).collect(Collectors.toList());
       return new ResponseEntity<>(lista, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Void> registrar(@Valid @RequestBody RolDTO dtoRequest) throws Exception{
        Rol r = mapper.map(dtoRequest, Rol.class);
        Rol obj = service.registrar(r);
        RolDTO dtoResponse = mapper.map(obj, RolDTO.class);
        URI location =  ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(dtoResponse.getIdRol()).toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping
    public ResponseEntity<RolDTO> modificar(@RequestBody RolDTO dtoRequest) throws Exception{
        Rol r = service.listarPorId(dtoRequest.getIdRol());
        if(r == null){
            throw new ModeloNotFoundException("ID NO ENCONTRADO" + dtoRequest.getIdRol());
        }
            Rol rol = mapper.map(dtoRequest, Rol.class);
            Rol obj =  service.modificar(rol);
            RolDTO dtoResponse = mapper.map(obj, RolDTO.class);
        return new ResponseEntity<>(dtoResponse, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception{
        Rol r = service.listarPorId(id);
        if(r == null){
            throw new ModeloNotFoundException("ID NO ENCONTRADO" +id);
        }
        service.eliminar(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{id}")
    public ResponseEntity<RolDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
        RolDTO dtoResponse;
        Rol obj = service.listarPorId(id);
        if(obj == null){
            throw new ModeloNotFoundException("ID NO ENCONTRADO" + id);
        }else{
        dtoResponse = mapper.map(obj, RolDTO.class);
        }
        return new ResponseEntity<>(dtoResponse, HttpStatus.OK);
    }
    
    @GetMapping("/pageable")
    public ResponseEntity<Page<RolDTO>> listarPageable(Pageable page) throws Exception{
        Page<RolDTO> roles = service.listarPageable(page).map(r -> mapper.map(r, RolDTO.class));
        return new ResponseEntity<>(roles, HttpStatus.OK);
    }
}
